#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QMessageBox>
#include <QFileDialog>
#include <QByteArray>
#include <QByteArrayMatcher>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void loadGSEfile(const QString &filename);

    void on_loadGSE_Button_clicked();

    void on_BBsearch_pushButton_clicked();

    void on_GSH3_pushButton_clicked();

    void on_BBdecode_pushButton_clicked();

private:
    Ui::MainWindow *ui;

    QByteArray ba;
    QByteArray nb;
   // QByteArray pattern;
    QByteArrayMatcher bam;
};

#endif // MAINWINDOW_H
