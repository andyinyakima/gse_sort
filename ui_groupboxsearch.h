/********************************************************************************
** Form generated from reading UI file 'groupboxsearch.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GROUPBOXSEARCH_H
#define UI_GROUPBOXSEARCH_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_GroupBoxSearch
{
public:

    void setupUi(QGroupBox *GroupBoxSearch)
    {
        if (GroupBoxSearch->objectName().isEmpty())
            GroupBoxSearch->setObjectName(QStringLiteral("GroupBoxSearch"));
        GroupBoxSearch->resize(400, 300);

        retranslateUi(GroupBoxSearch);

        QMetaObject::connectSlotsByName(GroupBoxSearch);
    } // setupUi

    void retranslateUi(QGroupBox *GroupBoxSearch)
    {
        GroupBoxSearch->setWindowTitle(QApplication::translate("GroupBoxSearch", "Search", 0));
        GroupBoxSearch->setTitle(QApplication::translate("GroupBoxSearch", "GroupBox", 0));
    } // retranslateUi

};

namespace Ui {
    class GroupBoxSearch: public Ui_GroupBoxSearch {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GROUPBOXSEARCH_H
