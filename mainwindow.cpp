/*
 * This program will input a file (during beta) that was saved as a generic stream
 * and analyze the BB headers and the GSE headers. It will endeavor to catagorize
 * and later to dispatch to specific streams.
 *
 *
 *
 * Free Copyleft started April 2014
 * by Andy Laberge
 * email: andylaberge@linux.com
 *
 */


#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadGSEfile(const QString &filename)
{
    QFile file(filename);

// Need to make notes on opening a binary file
// Weird things happen if you start linking it to text

    if(!file.open(QIODevice::ReadOnly)){
        QMessageBox::warning(this,tr("Recent Files"),
                             tr("Can not read file %1:\n%2.")
                             .arg(filename)
                             .arg(file.errorString()));
        return;
    }
    int i;
    int cnt;

     QDataStream in(&file);
    //  in.setVersion(15);   // doesn't seem necessary at this point
      quint8  byte;        // used with Alternate below
    while(in.atEnd()==false)
    {
        /* this method uses readRawdata which might be useful when data sizes change*/
       /*
         char *byte = new char[1];
        in.readRawData(byte,1);
         ba.append(byte,1);
        delete[] byte;
        */
        /* Alternate>> this is standard byte size data */
          in>>byte;
          ba.append(byte);
    }

 /* below will be use only if using a file
  * ba is a QByteArray
  * file is a QFile
  * readAll() places the whole file into the byte array
      // ba=file.readAll();
*/
   // int basz=ba.size();
    i=0;
    while(i<5000) // 5000 bytes is enough to view sync byte repition in GSE
    {
        for(cnt=0;cnt<=187;cnt++)
        {
            nb[0]=ba[i];
            ui->plainTextEdit_first->insertPlainText(nb.toHex()+" ");
            i++;
        }
        ui->plainTextEdit_first->insertPlainText("\n");
    }

    statusBar()->showMessage(tr("File loaded."),2000);

}


void MainWindow::on_loadGSE_Button_clicked()
{
    QString file_name = QFileDialog::getOpenFileName(this);
    if(!file_name.isEmpty())
        loadGSEfile(file_name);
}


void MainWindow::on_BBsearch_pushButton_clicked()
{

    ui->plainTextEdit_first->clear();
    QString str;
  //  QString hexline;
    QByteArray byte;
  //  QByteArray aba;
    QByteArray pat;
    QByteArray nba;

    str=ui->BBheader_lineEdit->text();
    byte=QByteArray::fromHex(str.toLocal8Bit());
    pat.append(byte);

    ui->plainTextEdit_first->appendPlainText("BB Header Search under way!");

   // bam.setPattern(pat);
    int pos = 0;
    int from =0;
    int cnt=0;
    int bynum=10;
    //int spinboxbyt=0;

    do
    {
    pos=ba.indexOf(pat,from+5);
    str=QString::number(pos);
    ui->plainTextEdit_first->appendPlainText(str+"\t");
    from=pos;
    cnt++;
    if(pos!=-1)
        {
        for(int i=0;i<bynum;i++)
            {
                nba[0]=ba[pos+i];
                ui->plainTextEdit_first->insertPlainText(" "+ nba.toHex());
            }
        }
    }while(pos!=-1);
    cnt--;
    ui->plainTextEdit_first->appendPlainText(QString::number(cnt));


 }

void MainWindow::on_GSH3_pushButton_clicked()
{
    ui->plainTextEdit_second->clear();
    QString str;
  //  QString hexline;
    QByteArray byte;
  //  QByteArray aba;
    QByteArray pat;
    QByteArray nba;

    str=ui->BBheader_lineEdit->text();
    byte=QByteArray::fromHex(str.toLocal8Bit());
    pat.append(byte);

    ui->plainTextEdit_second->appendPlainText("BB Header Search under way!");

   // bam.setPattern(pat);
    int pos = 0;
    int from =0;
    int cnt=0;
    int bynum=10;
    int spinboxbyt=1;
    spinboxbyt=ui->GSH_spinBox->value();

    do
    {
    pos=ba.indexOf(pat,from+5);
    str=QString::number(pos);
    ui->plainTextEdit_second->appendPlainText(str+"\t");
    from=pos;
    cnt++;
    if(pos!=-1)
        {
        for(int i=0;i<bynum+spinboxbyt;i++)
           {
                nba[0]=ba[pos+i];
                if(i==10)
                    ui->plainTextEdit_second->insertPlainText("\t\t"+ nba.toHex());
                else ui->plainTextEdit_second->insertPlainText(" "+ nba.toHex());
            }
        }
    }while(pos!=-1);
    cnt--;
    ui->plainTextEdit_second->appendPlainText(QString::number(cnt));


}

void MainWindow::on_BBdecode_pushButton_clicked()
{
    int patsz;
    QByteArray byte;
    QByteArray pat;
    QString str;
    ui->plainTextEdit_first->clear();

    str=ui->BBheader_lineEdit->text();
    byte=QByteArray::fromHex(str.toLocal8Bit());
    pat.append(byte);

    patsz=pat.size();
    ui->plainTextEdit_first->insertPlainText(QString::number(patsz));


}
