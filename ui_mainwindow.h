/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QPushButton *loadGSE_Button;
    QPushButton *BBsearch_pushButton;
    QPushButton *BBdecode_pushButton;
    QPushButton *GSH3_pushButton;
    QSpinBox *GSH_spinBox;
    QLabel *label;
    QLabel *label_2;
    QPlainTextEdit *plainTextEdit_first;
    QPlainTextEdit *plainTextEdit_second;
    QLineEdit *BBheader_lineEdit;
    QLineEdit *GSheader_lineEdit;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(744, 439);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMaximumSize(QSize(744, 675));
        MainWindow->setInputMethodHints(Qt::ImhNone);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        loadGSE_Button = new QPushButton(groupBox);
        loadGSE_Button->setObjectName(QStringLiteral("loadGSE_Button"));
        loadGSE_Button->setGeometry(QRect(10, 20, 121, 23));
        BBsearch_pushButton = new QPushButton(groupBox);
        BBsearch_pushButton->setObjectName(QStringLiteral("BBsearch_pushButton"));
        BBsearch_pushButton->setGeometry(QRect(10, 50, 121, 23));
        BBdecode_pushButton = new QPushButton(groupBox);
        BBdecode_pushButton->setObjectName(QStringLiteral("BBdecode_pushButton"));
        BBdecode_pushButton->setGeometry(QRect(10, 80, 121, 23));
        GSH3_pushButton = new QPushButton(groupBox);
        GSH3_pushButton->setObjectName(QStringLiteral("GSH3_pushButton"));
        GSH3_pushButton->setGeometry(QRect(10, 110, 121, 23));
        GSH_spinBox = new QSpinBox(groupBox);
        GSH_spinBox->setObjectName(QStringLiteral("GSH_spinBox"));
        GSH_spinBox->setGeometry(QRect(10, 140, 121, 24));
        GSH_spinBox->setMinimum(1);
        GSH_spinBox->setMaximum(13);
        GSH_spinBox->setValue(3);

        gridLayout->addWidget(groupBox, 0, 0, 3, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 2, 1, 1);

        plainTextEdit_first = new QPlainTextEdit(centralWidget);
        plainTextEdit_first->setObjectName(QStringLiteral("plainTextEdit_first"));

        gridLayout->addWidget(plainTextEdit_first, 1, 1, 1, 1);

        plainTextEdit_second = new QPlainTextEdit(centralWidget);
        plainTextEdit_second->setObjectName(QStringLiteral("plainTextEdit_second"));

        gridLayout->addWidget(plainTextEdit_second, 1, 2, 1, 1);

        BBheader_lineEdit = new QLineEdit(centralWidget);
        BBheader_lineEdit->setObjectName(QStringLiteral("BBheader_lineEdit"));
        sizePolicy.setHeightForWidth(BBheader_lineEdit->sizePolicy().hasHeightForWidth());
        BBheader_lineEdit->setSizePolicy(sizePolicy);
        BBheader_lineEdit->setMaxLength(30);
        BBheader_lineEdit->setFrame(true);
        BBheader_lineEdit->setClearButtonEnabled(true);

        gridLayout->addWidget(BBheader_lineEdit, 2, 1, 1, 1);

        GSheader_lineEdit = new QLineEdit(centralWidget);
        GSheader_lineEdit->setObjectName(QStringLiteral("GSheader_lineEdit"));
        sizePolicy.setHeightForWidth(GSheader_lineEdit->sizePolicy().hasHeightForWidth());
        GSheader_lineEdit->setSizePolicy(sizePolicy);
        GSheader_lineEdit->setMaxLength(39);
        GSheader_lineEdit->setFrame(true);
        GSheader_lineEdit->setClearButtonEnabled(true);

        gridLayout->addWidget(GSheader_lineEdit, 2, 2, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "gse_sort", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Push Button Actions", 0));
        loadGSE_Button->setText(QApplication::translate("MainWindow", "Load GSE file", 0));
        BBsearch_pushButton->setText(QApplication::translate("MainWindow", "BB Header Search", 0));
        BBdecode_pushButton->setText(QApplication::translate("MainWindow", "Decode BB Header", 0));
        GSH3_pushButton->setText(QApplication::translate("MainWindow", "Srch BBH + GSH  ", 0));
        GSH_spinBox->setSuffix(QApplication::translate("MainWindow", "  byte_search", 0));
        label->setText(QApplication::translate("MainWindow", "First Editor", 0));
        label_2->setText(QApplication::translate("MainWindow", "Second Editor", 0));
        BBheader_lineEdit->setInputMask(QApplication::translate("MainWindow", " HH HH HH HH HH HH HH HH HH HH", 0));
        BBheader_lineEdit->setText(QApplication::translate("MainWindow", " 42 00 00 00      ", 0));
        GSheader_lineEdit->setInputMask(QApplication::translate("MainWindow", " HH HH HH HH HH HH HH HH HH HH HH HH HH", 0));
        GSheader_lineEdit->setText(QApplication::translate("MainWindow", " c0            ", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
